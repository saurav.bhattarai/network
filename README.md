## Health Information System

### Objectives
* Knowledge dissemination on e-health
* To open new arena for IT students
* Promoting use of open source technologies

### Presentation
* [Guest Lecture Presentation](https://gitlab.com/ehealthnetworknepal/network/blob/master/Guest_Lecture_on_Health_Information_System.pdf)

### External Resources
* [DHIS2 Website](https://www.dhis2.org)
* [DHIS2 Documentation](https://www.dhis2.org/documentation)
* [DHIS2 Demo](https://play.dhis2.org/2.30/dhis-web-commons/security/login.action)
* [Asia eHealth Information Network](http://aehin.org/)

